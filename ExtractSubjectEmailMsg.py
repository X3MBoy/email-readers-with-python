#!/usr/bin/env python3

import extract_msg

import glob
file_list = glob.glob('*.msg')
for msg_file in file_list:
    mail = extract_msg.Message(msg_file)
    for att in mail.attachments:
        att.save()
files = glob.glob('**/*.txt',recursive = True)
for text in files:
    with open(text) as textfile:
        for line in textfile:
            if "Subject" in line:
                print(line)