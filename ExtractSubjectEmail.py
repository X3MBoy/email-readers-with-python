#!/usr/bin/env python

from email import policy
from email.parser import BytesParser
import glob
file_list = glob.glob('*.eml')
for eml_file in file_list:
    with open(eml_file, 'rb') as fp:
        msg = BytesParser(policy=policy.default).parse(fp)
    print('Subject:', msg['subject'])
